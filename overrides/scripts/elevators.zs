// change recipes for elevators

craftingTable.removeByName("elevatorid:elevator_white");
craftingTable.addShaped("elevatorid_elevator_white", <item:elevatorid:elevator_white>, [[<item:minecraft:white_wool>, <item:minecraft:white_wool>, <item:minecraft:white_wool>], [<item:minecraft:white_wool>, <item:botania:mana_pearl>, <item:minecraft:white_wool>], [<item:minecraft:white_wool>, <item:minecraft:white_wool>, <item:minecraft:white_wool>]]);

craftingTable.removeByName("elevatorid:elevator_orange");
craftingTable.addShaped("elevatorid_elevator_orange", <item:elevatorid:elevator_orange>, [[<item:minecraft:orange_wool>, <item:minecraft:orange_wool>, <item:minecraft:orange_wool>], [<item:minecraft:orange_wool>, <item:botania:mana_pearl>, <item:minecraft:orange_wool>], [<item:minecraft:orange_wool>, <item:minecraft:orange_wool>, <item:minecraft:orange_wool>]]);

craftingTable.removeByName("elevatorid:elevator_magenta");
craftingTable.addShaped("elevatorid_elevator_magenta", <item:elevatorid:elevator_magenta>, [[<item:minecraft:magenta_wool>, <item:minecraft:magenta_wool>, <item:minecraft:magenta_wool>], [<item:minecraft:magenta_wool>, <item:botania:mana_pearl>, <item:minecraft:magenta_wool>], [<item:minecraft:magenta_wool>, <item:minecraft:magenta_wool>, <item:minecraft:magenta_wool>]]);

craftingTable.removeByName("elevatorid:elevator_light_blue");
craftingTable.addShaped("elevatorid_elevator_light_blue", <item:elevatorid:elevator_light_blue>, [[<item:minecraft:light_blue_wool>, <item:minecraft:light_blue_wool>, <item:minecraft:light_blue_wool>], [<item:minecraft:light_blue_wool>, <item:botania:mana_pearl>, <item:minecraft:light_blue_wool>], [<item:minecraft:light_blue_wool>, <item:minecraft:light_blue_wool>, <item:minecraft:light_blue_wool>]]);

craftingTable.removeByName("elevatorid:elevator_yellow");
craftingTable.addShaped("elevatorid_elevator_yellow", <item:elevatorid:elevator_yellow>, [[<item:minecraft:yellow_wool>, <item:minecraft:yellow_wool>, <item:minecraft:yellow_wool>], [<item:minecraft:yellow_wool>, <item:botania:mana_pearl>, <item:minecraft:yellow_wool>], [<item:minecraft:yellow_wool>, <item:minecraft:yellow_wool>, <item:minecraft:yellow_wool>]]);

craftingTable.removeByName("elevatorid:elevator_lime");
craftingTable.addShaped("elevatorid_elevator_lime", <item:elevatorid:elevator_lime>, [[<item:minecraft:lime_wool>, <item:minecraft:lime_wool>, <item:minecraft:lime_wool>], [<item:minecraft:lime_wool>, <item:botania:mana_pearl>, <item:minecraft:lime_wool>], [<item:minecraft:lime_wool>, <item:minecraft:lime_wool>, <item:minecraft:lime_wool>]]);

craftingTable.removeByName("elevatorid:elevator_pink");
craftingTable.addShaped("elevatorid_elevator_pink", <item:elevatorid:elevator_pink>, [[<item:minecraft:pink_wool>, <item:minecraft:pink_wool>, <item:minecraft:pink_wool>], [<item:minecraft:pink_wool>, <item:botania:mana_pearl>, <item:minecraft:pink_wool>], [<item:minecraft:pink_wool>, <item:minecraft:pink_wool>, <item:minecraft:pink_wool>]]);

craftingTable.removeByName("elevatorid:elevator_gray");
craftingTable.addShaped("elevatorid_elevator_gray", <item:elevatorid:elevator_gray>, [[<item:minecraft:gray_wool>, <item:minecraft:gray_wool>, <item:minecraft:gray_wool>], [<item:minecraft:gray_wool>, <item:botania:mana_pearl>, <item:minecraft:gray_wool>], [<item:minecraft:gray_wool>, <item:minecraft:gray_wool>, <item:minecraft:gray_wool>]]);

craftingTable.removeByName("elevatorid:elevator_light_gray");
craftingTable.addShaped("elevatorid_elevator_light_gray", <item:elevatorid:elevator_light_gray>, [[<item:minecraft:light_gray_wool>, <item:minecraft:light_gray_wool>, <item:minecraft:light_gray_wool>], [<item:minecraft:light_gray_wool>, <item:botania:mana_pearl>, <item:minecraft:light_gray_wool>], [<item:minecraft:light_gray_wool>, <item:minecraft:light_gray_wool>, <item:minecraft:light_gray_wool>]]);

craftingTable.removeByName("elevatorid:elevator_black");
craftingTable.addShaped("elevatorid_elevator_black", <item:elevatorid:elevator_black>, [[<item:minecraft:black_wool>, <item:minecraft:black_wool>, <item:minecraft:black_wool>], [<item:minecraft:black_wool>, <item:botania:mana_pearl>, <item:minecraft:black_wool>], [<item:minecraft:black_wool>, <item:minecraft:black_wool>, <item:minecraft:black_wool>]]);

craftingTable.removeByName("elevatorid:elevator_red");
craftingTable.addShaped("elevatorid_elevator_red", <item:elevatorid:elevator_red>, [[<item:minecraft:red_wool>, <item:minecraft:red_wool>, <item:minecraft:red_wool>], [<item:minecraft:red_wool>, <item:botania:mana_pearl>, <item:minecraft:red_wool>], [<item:minecraft:red_wool>, <item:minecraft:red_wool>, <item:minecraft:red_wool>]]);

craftingTable.removeByName("elevatorid:elevator_green");
craftingTable.addShaped("elevatorid_elevator_green", <item:elevatorid:elevator_green>, [[<item:minecraft:green_wool>, <item:minecraft:green_wool>, <item:minecraft:green_wool>], [<item:minecraft:green_wool>, <item:botania:mana_pearl>, <item:minecraft:green_wool>], [<item:minecraft:green_wool>, <item:minecraft:green_wool>, <item:minecraft:green_wool>]]);

craftingTable.removeByName("elevatorid:elevator_brown");
craftingTable.addShaped("elevatorid_elevator_brown", <item:elevatorid:elevator_brown>, [[<item:minecraft:brown_wool>, <item:minecraft:brown_wool>, <item:minecraft:brown_wool>], [<item:minecraft:brown_wool>, <item:botania:mana_pearl>, <item:minecraft:brown_wool>], [<item:minecraft:brown_wool>, <item:minecraft:brown_wool>, <item:minecraft:brown_wool>]]);

craftingTable.removeByName("elevatorid:elevator_blue");
craftingTable.addShaped("elevatorid_elevator_blue", <item:elevatorid:elevator_blue>, [[<item:minecraft:blue_wool>, <item:minecraft:blue_wool>, <item:minecraft:blue_wool>], [<item:minecraft:blue_wool>, <item:botania:mana_pearl>, <item:minecraft:blue_wool>], [<item:minecraft:blue_wool>, <item:minecraft:blue_wool>, <item:minecraft:blue_wool>]]);

craftingTable.removeByName("elevatorid:elevator_purple");
craftingTable.addShaped("elevatorid_elevator_purple", <item:elevatorid:elevator_purple>, [[<item:minecraft:purple_wool>, <item:minecraft:purple_wool>, <item:minecraft:purple_wool>], [<item:minecraft:purple_wool>, <item:botania:mana_pearl>, <item:minecraft:purple_wool>], [<item:minecraft:purple_wool>, <item:minecraft:purple_wool>, <item:minecraft:purple_wool>]]);

craftingTable.removeByName("elevatorid:elevator_cyan");
craftingTable.addShaped("elevatorid_elevator_cyan", <item:elevatorid:elevator_cyan>, [[<item:minecraft:cyan_wool>, <item:minecraft:cyan_wool>, <item:minecraft:cyan_wool>], [<item:minecraft:cyan_wool>, <item:botania:mana_pearl>, <item:minecraft:cyan_wool>], [<item:minecraft:cyan_wool>, <item:minecraft:cyan_wool>, <item:minecraft:cyan_wool>]]);
