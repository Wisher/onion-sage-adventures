#loader crafttweaker

#priority 800

import crafttweaker.api.FurnaceManager;

// fix conflict with futura block
craftingTable.removeByName("computercraft:wired_modem");
craftingTable.addShaped("computercraft_wired_modem",
  <item:computercraft:wired_modem>,
  [
    [<tag:items:forge:stone>, <tag:items:forge:stone>, <tag:items:forge:stone>],
    [<tag:items:forge:stone>, <tag:items:forge:dusts/redstone>, <tag:items:forge:dusts/redstone>],
    [<tag:items:forge:stone>, <tag:items:forge:stone>, <tag:items:forge:stone>]
  ]
);

// add netherite debris to scrap recipe
craftingTable.addShaped("netherite_scrap",
  <item:minecraft:netherite_scrap>,
  [
    [<item:tconstruct:debris_nugget>, <item:tconstruct:debris_nugget>, <item:tconstruct:debris_nugget>],
    [<item:tconstruct:debris_nugget>, <item:tconstruct:debris_nugget>, <item:tconstruct:debris_nugget>],
    [<item:tconstruct:debris_nugget>, <item:tconstruct:debris_nugget>, <item:tconstruct:debris_nugget>]
  ]
);

//add cooking water rubber recipe
craftingTable.addShaped("thermal_rubber",
  <item:thermal:rubber>,
  [
    [<item:minecraft:vine>, <item:minecraft:vine>, <item:minecraft:vine>],
    [<item:minecraft:vine>, <tag:items:forge:water>, <item:minecraft:vine>],
    [<item:minecraft:vine>, <item:minecraft:vine>, <item:minecraft:vine>]
  ]
);

// add crushed uranium to furnace
// FurnaceManager.addRecipe(name as string, output as IItemStack, input as IIngredient, xp as float, cookTime as int) as void
furnace.addRecipe("crusheduranium2ingot", <item:bigreactors:yellorium_ingot>, <item:create:crushed_uranium_ore>, 0.6, 200);