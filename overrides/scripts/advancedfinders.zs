// change recipes of finders to just use repeaters
craftingTable.removeByName("adfinders:metal_finder");
craftingTable.removeByName("adfinders:mineral_finder");
craftingTable.removeByName("adfinders:gem_finder");

craftingTable.addShaped("adfinders_metal_finder",
  <item:adfinders:metal_finder>,
  [
    [<item:minecraft:repeater>, <tag:items:forge:glass_panes>, <item:minecraft:repeater>],
    [<tag:items:forge:ingots/gold>, <item:minecraft:compass>, <tag:items:forge:ingots/gold>],
    [<item:minecraft:repeater>, <tag:items:forge:ingots/gold>, <item:minecraft:repeater>]
  ]
);
craftingTable.addShaped("adfinders_mineral_finder",
  <item:adfinders:mineral_finder>,
  [
    [<item:minecraft:repeater>, <tag:items:forge:glass_panes>, <item:minecraft:repeater>],
    [<tag:items:forge:ingots/iron>, <item:minecraft:compass>, <tag:items:forge:ingots/iron>],
    [<item:minecraft:repeater>, <tag:items:forge:ingots/iron>, <item:minecraft:repeater>]
  ]
);
craftingTable.addShaped("adfinders_gem_finder",
  <item:adfinders:gem_finder>,
  [
    [<item:minecraft:repeater>, <tag:items:forge:glass_panes>, <item:minecraft:repeater>],
    [<tag:items:forge:gems/diamond>, <item:minecraft:compass>, <tag:items:forge:gems/diamond>],
    [<item:minecraft:repeater>, <tag:items:forge:gems/diamond>, <item:minecraft:repeater>]
  ]
);