
// change recipe for MKI
craftingTable.removeByName("mininggadgets:mininggadget_simple");
craftingTable.addShaped("mininggadget_simple",
  <item:mininggadgets:mininggadget_simple>,
  [
    [<tag:items:forge:gems/dragonstone>, <tag:items:forge:ingots/iron>, <tag:items:forge:ingots/gold>],
    [<tag:items:forge:gems/dragonstone>, <item:mininggadgets:upgrade_empty>, <tag:items:forge:dusts/redstone>],
    [<tag:items:forge:gems/dragonstone>, <tag:items:forge:ingots/iron>, <tag:items:forge:ingots/iron>]
  ]
);

// change recipe for MKI
craftingTable.removeByName("mininggadgets:mininggadget_fancy");
craftingTable.addShaped("mininggadget_fancy",
  <item:mininggadgets:mininggadget_fancy>,
  [
    [<tag:items:forge:gems/dragonstone>, <tag:items:forge:ingots/iron>, <tag:items:forge:ingots/iron>],
    [<tag:items:forge:gems/dragonstone>, <item:mininggadgets:upgrade_empty>, <tag:items:forge:dusts/redstone>],
    [<tag:items:forge:gems/dragonstone>, <tag:items:forge:ingots/iron>, <tag:items:forge:ingots/gold>]
  ]
);

// change recipe for MKI
craftingTable.removeByName("mininggadgets:mininggadget");
craftingTable.addShaped("mininggadget",
  <item:mininggadgets:mininggadget>,
  [
    [<tag:items:forge:gems/dragonstone>, <tag:items:forge:ingots/iron>, <tag:items:forge:ingots/gold>],
    [<tag:items:forge:gems/dragonstone>, <item:mininggadgets:upgrade_empty>, <tag:items:forge:dusts/redstone>],
    [<tag:items:forge:gems/dragonstone>, <tag:items:forge:ingots/iron>, <tag:items:forge:ingots/gold>]
  ]
);
