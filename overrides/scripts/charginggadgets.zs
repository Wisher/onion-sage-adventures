
// change recipe for charging station
craftingTable.removeByName("charginggadgets:charging_station");
craftingTable.addShaped("charging_station",
  <item:charginggadgets:charging_station>,
  [
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:dusts/redstone>, <tag:items:forge:ingots/manasteel>],
    [<tag:items:forge:gems/lapis>, <item:createaddition:tesla_coil>, <tag:items:forge:gems/lapis>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:storage_blocks/coal>, <tag:items:forge:ingots/manasteel>]
  ]
);