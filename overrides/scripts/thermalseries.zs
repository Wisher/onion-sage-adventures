
// Machine recipes
// change recipe of redstone furnace
craftingTable.removeByName("thermal:machine_furnace");
craftingTable.addShaped("redstone_machine_furnace",
  <item:thermal:machine_furnace>,
  [
    [<item:minecraft:air>, <tag:items:forge:dusts/mana>, <item:minecraft:air>],
    [<item:minecraft:bricks>, <item:thermal:machine_frame>, <item:minecraft:bricks>],
    [<tag:items:forge:gears/copper>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/copper>]
  ]
);

// change recipe of sawmill
craftingTable.removeByName("thermal:machine_sawmill");
craftingTable.addShaped("thermal_machine_sawmill",
  <item:thermal:machine_sawmill>,
  [
    [<item:minecraft:air>, <item:thermal:saw_blade>, <item:minecraft:air>],
    [<tag:items:botania:livingrock>, <item:thermal:machine_frame>, <tag:items:botania:livingrock>],
    [<tag:items:forge:gears/copper>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/copper>]
  ]
);

// change recipe of pulverizer
craftingTable.removeByName("thermal:machine_pulverizer");
craftingTable.addShaped("machine_pulverizer",
  <item:thermal:machine_pulverizer>,
  [
    [<item:minecraft:air>, <item:botania:piston_relay>, <item:minecraft:air>],
    [<item:minecraft:flint>, <item:thermal:machine_frame>, <item:minecraft:flint>],
    [<tag:items:forge:gears/copper>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/copper>]
  ]
);

// change recipe of induction smelter
craftingTable.removeByName("thermal:machine_smelter");
craftingTable.addShaped("induction_machine_smelter",
  <item:thermal:machine_smelter>,
  [
    [<item:minecraft:air>, <item:botania:endoflame>, <item:minecraft:air>],
    [<tag:items:forge:sand>, <item:thermal:machine_frame>, <tag:items:forge:sand>],
    [<tag:items:forge:gears/invar>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/invar>]
  ]
);

// change recipe of phytogen insolator
craftingTable.removeByName("thermal:machine_insolator");
craftingTable.addShaped("phytogetic_machine_insolator",
  <item:thermal:machine_insolator>,
  [
    [<item:minecraft:air>, <item:minecraft:dirt>, <item:minecraft:air>],
    [<item:botania:mana_glass>, <item:thermal:machine_frame>, <item:botania:mana_glass>],
    [<tag:items:forge:gears/lumium>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/lumium>]
  ]
);

// change recipe for centrifuge
craftingTable.removeByName("thermal:machine_centrifuge");
craftingTable.addShaped("machine_centrifuge",
  <item:thermal:machine_centrifuge>,
  [
    [<item:minecraft:air>, <item:botania:rune_wrath>, <item:minecraft:air>],
    [<tag:items:forge:ingots/tin>, <item:thermal:machine_frame>, <tag:items:forge:ingots/tin>],
    [<tag:items:forge:gears/constantan>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/constantan>]
  ]
);

// change recipe for multiservo press
craftingTable.removeByName("thermal:machine_press");
craftingTable.addShaped("multiservo_machine_press",
  <item:thermal:machine_press>,
  [
    [<item:minecraft:air>, <tag:items:forge:storage_blocks/manasteel>, <item:minecraft:air>],
    [<tag:items:forge:ingots/bronze>, <item:thermal:machine_frame>, <tag:items:forge:ingots/bronze>],
    [<tag:items:forge:gears/constantan>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/constantan>]
  ]
);

//change recipe of magma crucible
craftingTable.removeByName("thermal:machine_crucible");
craftingTable.addShaped("magma_machine_crucible",
  <item:thermal:machine_crucible>,
  [
    [<item:minecraft:air>, <item:botania:mana_glass>, <item:minecraft:air>],
    [<item:minecraft:nether_bricks>, <item:thermal:machine_frame>, <item:minecraft:nether_bricks>],
    [<tag:items:forge:gears/invar>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/invar>]
  ]
);

// change recipe of blast chiller
craftingTable.removeByName("thermal:machine_chiller");
craftingTable.addShaped("thermal_machine_chiller",
  <item:thermal:machine_chiller>,
  [
    [<item:minecraft:air>, <item:botania:mana_glass>, <item:minecraft:air>],
    [<item:minecraft:packed_ice>, <item:thermal:machine_frame>, <item:minecraft:packed_ice>],
    [<tag:items:forge:gears/invar>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/invar>]
  ]
);

// change recipe for fractionating still
craftingTable.removeByName("thermal:machine_refinery");
craftingTable.addShaped("thermal_machine_refinery",
  <item:thermal:machine_refinery>,
  [
    [<item:minecraft:air>, <item:botania:mana_glass>, <item:minecraft:air>],
    [<tag:items:forge:ingots/copper>, <item:thermal:machine_frame>, <tag:items:forge:ingots/copper>],
    [<tag:items:forge:gears/invar>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/invar>]
  ]
);

// change recipe for pyrolyzer
craftingTable.removeByName("thermal:machine_pyrolyzer");
craftingTable.addShaped("thermal_machine_pyrolyzer",
  <item:thermal:machine_pyrolyzer>,
  [
    [<item:minecraft:air>, <tag:items:botania:runes/fire>, <item:minecraft:air>],
    [<item:minecraft:nether_bricks>, <item:thermal:machine_frame>, <item:minecraft:nether_bricks>],
    [<tag:items:forge:gears/constantan>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/constantan>]
  ]
);

// change recipe for fluid encapsulator
craftingTable.removeByName("thermal:machine_bottler");
craftingTable.addShaped("thermal_machine_bottler",
  <item:thermal:machine_bottler>,
  [
    [<item:minecraft:air>, <item:minecraft:bucket>, <item:minecraft:air>],
    [<item:botania:mana_glass>, <item:thermal:machine_frame>, <item:botania:mana_glass>],
    [<tag:items:forge:gears/copper>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/copper>]
  ]
);

// change recipe for alchemical imbiber
craftingTable.removeByName("thermal:machine_brewer");
craftingTable.addShaped("thermal_machine_brewer",
  <item:thermal:machine_brewer>,
  [
    [<item:minecraft:air>, <item:minecraft:brewing_stand>, <item:minecraft:air>],
    [<item:botania:mana_glass>, <item:thermal:machine_frame>, <item:botania:mana_glass>],
    [<tag:items:forge:gears/constantan>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/constantan>]
  ]
);

// change recipe for sequential fabricator
craftingTable.removeByName("thermal:machine_crafter");
craftingTable.addShaped("thermal_machine_crafter",
  <item:thermal:machine_crafter>,
  [
    [<item:minecraft:air>, <item:botania:crafty_crate>, <item:minecraft:air>],
    [<tag:items:forge:ingots/tin>, <item:thermal:machine_frame>, <tag:items:forge:ingots/tin>],
    [<tag:items:forge:gears/copper>, <item:createaddition:tesla_coil>, <tag:items:forge:gears/copper>]
  ]
);



// change recipe of blocks
// change recipe of workbench
craftingTable.removeByName("thermal:tinker_bench");
craftingTable.addShaped("thermal_tinker_bench",
  <item:thermal:tinker_bench>,
  [
    [<tag:items:forge:ingots/iron>, <tag:items:forge:ingots/iron>, <tag:items:forge:ingots/iron>],
    [<tag:items:forge:glass>, <item:botania:crafty_crate>, <tag:items:forge:glass>],
    [<tag:items:minecraft:planks>, <item:thermal:rf_coil>, <tag:items:minecraft:planks>]
  ]
);

// change recipe of energetic infuser
craftingTable.removeByName("thermal:charge_bench");
craftingTable.addShaped("thermal_charge_bench",
  <item:thermal:charge_bench>,
  [
    [<tag:items:forge:ingots/electrum>, <tag:items:forge:ingots/electrum>, <tag:items:forge:ingots/electrum>],
    [<item:thermal:rf_coil>, <tag:items:forge:storage_blocks/mana>, <item:thermal:rf_coil>],
    [<tag:items:forge:ingots/lead>, <item:createaddition:tesla_coil>, <tag:items:forge:ingots/lead>]
  ]
);



// Change item recipes
// change recipe for Flux Capacitor
craftingTable.removeByName("thermal:flux_capacitor");
craftingTable.addShaped("thermal_flux_capacitor",
  <item:thermal:flux_capacitor>,
  [
    [<tag:items:forge:dusts/mana>, <tag:items:forge:ingots/lead>, <tag:items:forge:dusts/mana>],
    [<tag:items:forge:ingots/lead>, <item:createaddition:tesla_coil>, <tag:items:forge:ingots/lead>],
    [<item:minecraft:air>, <tag:items:forge:dusts/mana>, <item:minecraft:air>]
  ]
);

// change recipe of the sawblade
craftingTable.removeByName("thermal:saw_blade");
craftingTable.addShaped("thermal_saw_blade",
  <item:thermal:saw_blade>,
  [
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:ingots/manasteel>, <item:minecraft:air>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:ingots/copper>, <tag:items:forge:ingots/manasteel>],
    [<item:minecraft:air>, <tag:items:forge:ingots/manasteel>, <tag:items:forge:ingots/manasteel>]
  ]
);

// change recipe of the fluxo magnet
craftingTable.removeByName("thermal:flux_magnet");
craftingTable.addShaped("thermal_flux_magnet",
  <item:thermal:flux_magnet>,
  [
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:dusts/mana>, <tag:items:forge:ingots/manasteel>],
    [<tag:items:forge:ingots/lead>, <tag:items:forge:ingots/manasteel>, <tag:items:forge:ingots/lead>],
    [<item:minecraft:air>, <item:thermal:rf_coil>, <item:minecraft:air>]
  ]
);

// change recipe of the drill head
craftingTable.removeByName("thermal:drill_head");
craftingTable.addShaped("thermal_drill_head",
  <item:thermal:drill_head>,
  [
    [<item:minecraft:air>, <tag:items:forge:ingots/manasteel>, <item:minecraft:air>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:ingots/copper>, <tag:items:forge:ingots/manasteel>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:ingots/manasteel>, <tag:items:forge:ingots/manasteel>]
  ]
);