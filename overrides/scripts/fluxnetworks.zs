
// change recipe for flux core
craftingTable.removeByName("fluxnetworks:fluxcore");
craftingTable.addShaped("fluxcore",
  <item:fluxnetworks:flux_core> * 4,
  [
    [<item:fluxnetworks:flux_dust>, <item:minecraft:obsidian>, <item:fluxnetworks:flux_dust>],
    [<item:minecraft:obsidian>, <item:botania:mana_pearl>, <item:minecraft:obsidian>],
    [<item:fluxnetworks:flux_dust>, <item:minecraft:obsidian>, <item:fluxnetworks:flux_dust>]
  ]
);

// change recipe for flux storage
craftingTable.removeByName("fluxnetworks:basicfluxstorage");
craftingTable.addShaped("basicfluxstorage",
  <item:fluxnetworks:basic_flux_storage>,
  [
    [<item:fluxnetworks:flux_block>, <item:fluxnetworks:flux_block>, <item:fluxnetworks:flux_block>],
    [<tag:items:forge:glass_panes>, <tag:items:forge:dusts/mana>, <tag:items:forge:glass_panes>],
    [<item:fluxnetworks:flux_block>, <item:fluxnetworks:flux_block>, <item:fluxnetworks:flux_block>]
  ]
);

// change recipe for flux controller
craftingTable.removeByName("fluxnetworks:fluxcontroller");
craftingTable.addShaped("fluxcontroller",
  <item:fluxnetworks:flux_controller>,
  [
    [<item:fluxnetworks:flux_block>, <item:fluxnetworks:flux_core>, <item:fluxnetworks:flux_block>],
    [<item:fluxnetworks:flux_dust>, <tag:items:forge:ingots/terrasteel>, <item:fluxnetworks:flux_dust>],
    [<item:fluxnetworks:flux_block>, <item:thermal:energy_cell>, <item:fluxnetworks:flux_block>]
  ]
);