import mods.itemstages.ItemStages;

// disable FE generation
craftingTable.removeByName("miniutilities:solar_panel_controller");
craftingTable.removeByName("miniutilities:solar_panel");
craftingTable.removeByName("miniutilities:lunar_panel");

// restrict angel ring flight
ItemStages.restrict(<tag:items:forge:angelring>, "flight_angelring");

// change recipe for healing axe
craftingTable.removeByName("miniutilities:healing_axe");
craftingTable.addShaped("miniutilities_healing_axe",
  <item:miniutilities:healing_axe>,
  [
    [<item:miniutilities:unstable_ingot>, <item:draconicevolution:awakened_core>],
    [<item:miniutilities:unstable_ingot>, <item:minecraft:obsidian>],
    [<item:minecraft:air>, <item:minecraft:obsidian>]
  ]
);

// add ender pearl block to ender pearls recipe
craftingTable.addShapeless("miniutil_enderblock_eye",
  <item:minecraft:ender_pearl> * 9,
  [
    <tag:items:forge:storage_blocks/ender_pearl>
  ]
);