// change recipe for fusion crafting injector
craftingTable.removeByName("draconicevolution:basic_crafting_injector");
craftingTable.addShaped("draconicevolution_basic_crafting_injector",
  <item:draconicevolution:basic_crafting_injector>,
  [
    [<tag:items:forge:nuggets/alfsteel>, <item:draconicevolution:draconium_core>, <tag:items:forge:nuggets/alfsteel>],
    [<tag:items:forge:stone>, <tag:items:forge:storage_blocks/enderium>, <tag:items:forge:stone>],
    [<tag:items:forge:stone>, <item:botania:conjuration_catalyst>, <tag:items:forge:stone>]
  ]
);