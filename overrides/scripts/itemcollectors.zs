// change item collectors recipes
craftingTable.removeByName("itemcollectors:basic_collector");
craftingTable.removeByName("itemcollectors:advanced_collector");

craftingTable.addShaped("itemcollectors_basic_collector",
  <item:itemcollectors:basic_collector>,
  [
    [<item:minecraft:air>, <item:botania:mana_pearl>, <item:minecraft:air>],
    [<item:minecraft:air>, <tag:items:forge:obsidian>, <item:minecraft:air>],
    [<tag:items:forge:obsidian>, <tag:items:forge:obsidian>, <tag:items:forge:obsidian>]
  ]
);
craftingTable.addShaped("itemcollectors_advanced_collector",
  <item:itemcollectors:advanced_collector>,
  [
    [<item:minecraft:air>, <tag:items:botania:runes/mana>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:itemcollectors:basic_collector>, <item:minecraft:air>],
    [<tag:items:forge:obsidian>, <tag:items:forge:obsidian>, <tag:items:forge:obsidian>]
  ]
);