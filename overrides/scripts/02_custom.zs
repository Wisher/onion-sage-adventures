#loader crafttweaker

#priority 900

// andesite recipes
craftingTable.addShaped("andesite_crafted",
  <item:minecraft:andesite> * 2,
  [
    [<item:minecraft:stone>, <tag:items:osa:andesite_recipe>]
  ]
);