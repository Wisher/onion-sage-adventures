// remove altar recipes
craftingTable.removeByName("mysticalagriculture:infusion_altar");
craftingTable.removeByName("mysticalagriculture:infusion_pedestal");

// change altar recipes
craftingTable.addShaped("mysticalagriculture_infusion_altar",
  <item:mysticalagriculture:infusion_altar>,
  [
    [<tag:items:forge:ingots/gold>, <item:draconicevolution:wyvern_core>, <tag:items:forge:ingots/gold>],
    [<item:extrabotany:sinrune>, <item:minecraft:stone>, <item:extrabotany:sinrune>],
    [<item:minecraft:stone>, <item:minecraft:stone>, <item:minecraft:stone>]
  ]
);
craftingTable.addShaped("mysticalagriculture_infusion_pedestal",
  <item:mysticalagriculture:infusion_pedestal>,
  [
    [<tag:items:forge:ingots/gold>, <item:draconicevolution:draconium_core>, <tag:items:forge:ingots/gold>],
    [<item:minecraft:air>, <item:minecraft:stone>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:stone>, <item:minecraft:air>]
  ]
);