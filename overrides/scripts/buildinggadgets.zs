
// change recipe for building gadget
craftingTable.removeByName("buildinggadgets:gadget_building");
craftingTable.addShaped("gadget_building",
  <item:buildinggadgets:gadget_building>.withTag(
    {state: {serializer: "buildinggadgets:dummy_serializer" as string, state: {Name: "minecraft:air" as string}, data: {}}}),
  [
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:dusts/redstone>, <tag:items:forge:ingots/manasteel>],
    [<tag:items:forge:gems/mana_diamond>, <item:createaddition:tesla_coil>, <tag:items:forge:gems/mana_diamond>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:gems/lapis>, <tag:items:forge:ingots/manasteel>]
  ]
);

// change recipe for destruction gadget
craftingTable.removeByName("buildinggadgets:gadget_destruction");
craftingTable.addShaped(
  "gadget_destruction",
  <item:buildinggadgets:gadget_destruction>.withTag({overlay: 0 as byte, fuzzy: 1 as byte}),
  [
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:dusts/redstone>, <tag:items:forge:ingots/manasteel>],
    [<item:botania:mana_pearl>, <item:createaddition:tesla_coil>, <item:botania:mana_pearl>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:gems/lapis>, <tag:items:forge:ingots/manasteel>]
  ]
);

// change recipe for copy-paste gadget
craftingTable.removeByName("buildinggadgets:gadget_copy_paste");
craftingTable.addShaped(
  "gadget_copy_paste",
  <item:buildinggadgets:gadget_copy_paste>.withTag({mode: 0 as byte, template_id: [1457428681, -85505670, -1084713533, 1420609349]}),
  [
    [<tag:items:forge:gems/emerald>, <tag:items:forge:dusts/redstone>, <tag:items:forge:gems/emerald>],
    [<item:botania:mana_pearl>, <item:createaddition:tesla_coil>, <item:botania:mana_pearl>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:gems/lapis>, <tag:items:forge:ingots/manasteel>]
  ]
);

// change recipe for exchanging gadget
craftingTable.removeByName("buildinggadgets:gadget_exchanging");
craftingTable.addShaped("gadget_exchanging",
  <item:buildinggadgets:gadget_exchanging>.withTag(
      {state: {serializer: "buildinggadgets:dummy_serializer" as string, state: {Name: "minecraft:air" as string}, data: {}}}),
  [
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:dusts/redstone>, <tag:items:forge:ingots/manasteel>],
    [<tag:items:forge:gems/mana_diamond>, <item:create:precision_mechanism>, <tag:items:forge:gems/mana_diamond>],
    [<tag:items:forge:ingots/manasteel>, <tag:items:forge:gems/lapis>, <tag:items:forge:ingots/manasteel>]
  ]
);