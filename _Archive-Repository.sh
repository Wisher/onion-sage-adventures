#!/bin/bash

## ---------- Parameter flags ---------- ##
# -v for version if pre-chosen
# -f for filename if pre-chosen
version=''
outfile=''

while getopts v:f: flags
do
    case ${flags} in
    v) version=${OPTARG};;
    f) outfile=${OPTARG};;
    esac
done

## ---------- Constants ---------- ##

CONTENTS=(
    './manifest.json'
    './modlist.html'
    './overrides'
)

# echo ${CONTENTS[@]}

## ---------- Logic ---------- ##
if ['' = $outfile]
then
  OSA='Onion Sage Adventures'
  echo Version:
  read version
  outfile="${OSA}-${version}"
fi

if ['' = $version]
then
  outfile="${OSA}-gitlab-export"
fi

destination="./${outfile}.zip"

zip -r "${destination}" "${CONTENTS[@]}"
