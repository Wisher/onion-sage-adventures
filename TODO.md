> >- Every Version TODOs
> >   - Manifest.json
> >   - Paste test client configs in to repo, make sure correct.
> >   - `Server` Bat files/update modpack version number
> >   - `Server` server.properties MoTD
>

- TODO: **make a java app to compare configs and offer to delete non existing ones**
- TODO: find out what makes worlds complain about experimental settings and disable
- TODO: make a script which deletes unnecessary .gitignored files so that exports from the repo don't include them (e.g. modinfo files are things users might edit and don't need to be included)
- TODO: edit `Journeymap` config to show claims for ftb chunks by default (done?)
- TODO: `patchouli` guidebook for the modpack, client extras should contain the additional information it needs
- TODO: **worldgen**:
  -  disable `defaultconfigs/thermal-server.toml` generation and manually set gens in worldgen mod
  -  disable `config/mekanism/world.toml` generation so manual generation can be done
  -  disable `config/create-common.toml` generation and implement manually
  -  check to ensure that ore spawning works as expecting in other dimensions
  -  check to ensure ores are spawning correctly, are other enabled mods spawning as expected, does quark spawn its stones etc. right?
- TODO: find a mod that has **XP storage**, a way to store and retrieve xp
- TODO: `waystone` given through quests
- TODO: add compacting rules for `storagedrawers` compacting thingy in config
  - quark compressed blocks
  - thermal cultivation compressed crops
- TODO: disable `waystones` in appropriate dimensions
  - appropriately adjust frequency of waystones (they should be rare, so reduce most likely)
  - add custom waystone names, adjust naming to be mixed?
  - look at waystones server config more, leashed mob blacklist will probably need to be expanded
- TODO: figure out why hunger is displayed twice on tooltips, and try to only have the one that shows both hunger and saturation appear (WAILA, HWYLA, WAILA integreation, jeiintegration are all possibilites (jei integration shows text only *ew*))
  - TODO: `tooltips` want to see redstone `WAILA`, may need to swap to The One Probe & TOP Addons, see if i can configure it too show as much as waila/hwyla does currently
- CC: Tweaked, ship extra programs which are useful to players with the modpack, also have programs as quest rewards
- TODO: `CoFH` mods, carefully decide which too keep
  - Similarly, scrutinise `ensorcellation`, disable any undesired enchantments (most likely keep)
- TODO: `quark` stuff
  - endermites form shulkers, make a quest teaching players about this
  - add iron shulker boxes to quarks shulker box list (and any others that end up in the modpack)
  - check trowel has durability, config is confusing, may be unbreakable
  - hoe harvesting, if other mods have hoes which work in a 3x3 or 5x5 this should be disabled to prefer those
  - document how quark stone clusters spawn (diorite, andesite, etc. info is useful for mods like create)
  - quark makes deepslate and smooth basalt spawn at bottom of world, may affect ore generation, if too troublesome, disable
  - consider enabling pipes for early-mid game usage if no suitable pipes mods are found, but focus more on having create/better player systems (storage drawers) handling this
  - **Quests:**
    - burn vine tips, quest teaching players about this mechanic
    - hedges (quest to make and to put flowers on them)
    - iron grate
    - glass item frames (show off their features, invisible with items, acts as wallpaper with banners etc.)
    - quark candles (TODO: find the mod that adds candles for enchanting table stuff that quark integrates with)
      - quest about how candles affect enchanting^
    - turf (waterlogged can grow sugarcane and hold any other item grass can)
    - chests in boats quest
    - inventory sharing (shift+*chat open button* links the item you are hovering over into chat)
    - right-click items into shulker boxes
    - play music for crabs
    - crabs grow from damage? (heal and damage them to grow them)
    - crabs bred with wheat/chicken/fish
    - frogs bred with spider eyes/fish
    - find a stoneling, get a heart of diamond and tame a stoneling
    - tortoise, can mine them, can carry them home, feeding them cave roots sometimes regens their ores
    - wraiths, right clicking with sould bead reveales closest nether fortress (like eyes of ender)
    - ancient tomes (overlevelled enchantments)
    - bottled cloud (how to collect and its usefulness)
    - skull pike (early game monster repellent)
    - slime bucket
    - trowel
    - campfires boost elytra (if its kept)
    - compasses work everywhere
    - infinity water bucket
    - lava buket trash
    - note block mob sounds
    - pat the dogs (patting wolves)
    - poison potatoes make baby mobs stay as babies
    - snow golem player heads (named snow golems drop the equivalent players head)
    - villagers follow emeralds
    - cave roots grow in the dark
    - fairy rings show emeralds/diamond (TODO: test if still works with customised ore generation)
    - underground biomes (time to go spelunking!)
    - backpack
    - matrix enchanting!
    - totem of holding/soul compass (can be located with a soul compass, breaks if you die a 2nd time before collecting it)
- TODO: `CoFh`
  - Locomotion
    - Quests and rewards for using the rails, illuminated rails, underwater rails/carts. Could integrate with other mods like botania spectral rail
      - could offer rails and materials for making railways as quest rewards (repeatable quests so players can indefintely build a rail system even early in progression?)
  - Tools/Machinery/Upgrades need to be balanced out for progression. Probably want to ideally use create(/tinkers?) mostly early game then move into more techy mods later
    - quest informing feather falling stops crop damage
  - Ensorcellation
    - consider re-enabling volley (3 arrows fired in arc, for price of 1 arrow), depends on what tinkers offers with bows, and what other bows / enchantments exist. Don't want this overly prioritised
    - TODO: consider increasing max levels of vanilla overridden enchants, to allow for more powerful enchantments (keep in mind tomes increase the level too)
    - **Quests:**
      - outlaw makes villages/golems drop emeralds aswell as increasing damage to Illagers
      - vigilante, increased damage to Illagers
      - hunters bounty, extra drops chance for bows (i think it increases drops? what does this do vs looting,can both be applied?)
      - gourmand, food is better
      - ender disruption,  inhibits teleportation of ender mobs, deal extra damage to ender mobs
      - vitality, increases hearts
      - vorpal, increased head drop chance, critical hit chance (extra damage) TODO: check that it goes to level 5 with quark, check level 5 has 50% head drop rate
      - insight, increased XP gain
      - reach, increased interaction distance
      - air affinity, normal interaction on ladders/while flying with mining etc.
      - phalanx, inscreased movement speed with shields
      - soulbound, TODO: test to see if the levels decrease, want this option more costly than other possibilities (if they exist)
- TODO: `randompatches` check if the boat underwater ejection is functioning correctly, does it interfere with underwater thermal minecarts?
- TODO: `Building Gadgets`, `Charging Gadgets` - **quests**
- TODO: `comforts` hammock/sleeping bag quests
- TODO: `akashic tome` ensure all books/items (that work as books, e.g. draconic tablet) for mod information are listed, and ensure appropriate alias's are in place (e.g. add on mods for botania should redirect to botania unless they have their own book)
- TODO: `cooking for blockheads`
  - consider enabling ovenRequiresCookingOil, makes oven only accepts Pams cooking oil to cook. Depending on how easy cooking oil is to obtain
  - **Quests:**
    - squish a cow, with an anvil, into a jar to make a milk jar/cow in a jar
- TODO: `randompatches`, custom window titles and images. Make OSA logos for images, change titles to include OSA info
- TODO: `apotheosis`, only enable farming and potion modules for now, investigate the deadly module to see if i can control the loot tables, only want the weapons as possible quest rewards. Also means bosses need to have some level of configuration too, don't want too strong of weapons being handed out
  - disable endless quiver enchantment from potions module, check out what the enchanted apple does (and do any other mods add an enchanted apple, conflicts?)
  - what are affixes? `config/apotheosis/affixes.cfg`
  - **Quests:**
    - Potion charms, last 4x longer for cost of 3 potions, can be used intermittently (removing stopps usage)
    - taller plants description
    - knowledge/sundering potions (sundering is good as a weapon to use on mobs)
- TODO: macaws bridges, test thermal locomotion rails, do the blocks work underwater? do locomotion rails attatch
  - **Quests:** for making bridges/rails/stair bridges
- TODO: balance `create`, `create crafts & additions` to stagger access to items where appropriate
  - **quest** for charging certus quartz in AE2 via the tesla coil
  - ensure charging stuff is set to the appropriate time in the game to access, don't want electricity too early, much of crafts and additions will probably be delayed in the pack (though still the early energy production/usage)
  - **quest** for fireworks in mechanical crafter (can use more than 9 ingredients)
  - **quests allround for create and createAddtions**
  - what is refined radiance and shadow steel?
  - figure out how schematics and cannons and work, and balance them
- TODO: `featuredservers` **add official server once it's running**
- TODO: `createaddition` balance FE, and max stress for alternator & electric motor, max rpm, energy consumption, max input, efficiency and others. Go through whole config for balancing
- TODO: **`Default Options`** setup keybinds, default servers, audio balances, etc. (run command `/defaultoptions saveAll` to export to folder)
  -TODO: **RECIPIES**
  - thermal series ingots center nugget is thermal only, should be ore-dicted
- TODO: check loot tables for mods and adjust them for balance, e.g. `repurposed structures` (which has many overworld thingies)
- TODO: yungs `repurposed structures` will need **data packs** for mod compatability (including with other yungs stuff)
- TODO: **`Yungs` mods**
  - ensure loot tables are working correctly for chests, e.g. can't find quark tomes anywhere, should have definitely been in the stronghold. These are important
    - this needs to be done via datapack, will need to assess what items spawn without yungs and what items spawn with yungs and then make my own table, should allso do so for structures other than strongholds such as desert temples
    - **repurposed structures** has references for data pack loot tables linked in its curseforge page, including pre-done ones for compat with 
  - are strongholds spawning the same way as vanilla (in rings?) do eyes of ender find them, do eyes of ender land over the portal frame?
  - check out the structures with the /locate command, should get a little familiar with it all
- TODO: `drawerfps` set a draw distance, one chunk?, two? whats the default
- TODO: `openblocks elevators` make require botania mana pearls in recipe
  - quest to show need of mana pearls
- TODO: `cc: tweaked` 
  - **quests** showing how to utilize
  - **rewards** programs i've premade and saved to floppys for rewards to do stuff
- TODO: `curio of undying` **quest** to inform players they can equip to the curio slot
- TODO: `recipe stages, game stages` **SET THESE UP**
- TODO: `findme` quest describing how to use this mod
- TODO: `custom starter gear` setup custom starting gear, akashic tome etc. maybe a backpack and compass
- TODO: `simple discord rpc` configure, refer to mod page
- TODO: `crash utilities` use to test out world data
- TODO: `enchanced celetials` **quests** describing mod functionality
- TODO: `nether exoticism` **quests** describing mod items
- TODO: `toolstats` consider removing, duplicates some info, but also adds some1
- TODO: configure `mineTogether`
- TODO: `featured servers` setup
- TODO: configure `world border`, disable in single players, **test** is not disabled in multiplayer
- TODO: `blue skies` **quests** informing player of existence and progression
- TODO: `farmers delight/nethers delight` **quest** to make the foods and inform player of the spice of life benefits
- TODO: review `supplementaries` items, ensure nothing is OP
- TODO: `wall jump` **quest** informing players of existance
- TODO: `ftb mods` configure
- TODO: cowjar **quest** (anvil a cow into a milk jar)
- TODO: storage drawers compacting config items, refer to compacting drawers configs from osa 1.12qq
- TODO: akashic tome
  - spawn with most books in the tome
- ​	TODO: make marble tagged as forge stone
- TODO: tinkers books (all 4) add to akashic tome
- TODO: test oredicted recipe for storage drawers
- TODO: jaopca glowstone 3x3, either remove or change recipe to something else
- 



> Goals:
>
> * about 80ish gameplay mods, not including my own ones
> * world gen mods not included, probably try to keep under 20
> * QoL mods not included in count, whatever works nicely there
> * mods that don't need to be in, rendering, some QoL etc. can go in *Client Additions*
> * setup forge config for default world types, failing that working properly there is a mod **Default World Type [forge]**
>
> >- **MODS TO ADD:**
> >- 


> >- TODO: - Official Server (after modpack release)
> >   -
> >- TODO:
> >  -
> >- TODO:
> >  -

1.14/15 MODPACK
different sized veins at different world heights, will need multiple entries for vein spawning to do this, maybe play with rarity too
Try to be compatible with ~~sponge/spongeforge~~ [MohistMC](https://mohistmc.com/download/), optifine, vivecraft,
    This will allow for plugins aswell as mods (towny for example, economy stuff etc.)
    May have to use `GriefPrevention` for the towny mod, it has the ability to make claims and then make subclaims, and sell claims? It advertises towny like stuff. Would need to make it so players cant claim with FTBUtils if going this route, also need to test out how well GriefPrevention works with forge mods
      Probably have currency in multiple forms, digital and physical, the physical currency would be made through mod items, i could make a mod with physical banks too which could slowly return interest at variable rates (thresholdes depending on amount in bank etc.), then the digital currency could be purely obtained through converting physical to digital (ie no banks for digital), digital currency could be used to buy claims or other plugin related purchases, this would tie everything back to the modded immersive experience and make use of mod items to obtain currency, all trade would stem from ingame items essentially.
            Currency could be exchanged through a buy/sell signshop in a server administration building, or i could make a mod which does this through commands or something, signshop would be the easy route


TODO: **`Dev Mode:`**
A mode for modpack/mod development, testing, troubleshooting
A script should be able to be run which can swap out versions of configs for a more useful development version
e.g. more info from waila about blocks you are looking at (states etc.), quest editing mode by default, debug info
  - waila_plugins.json
    - enable `show_registry`, `show_states`
  - jei-client.toml
    - debug mode enable? (what does it do)
  - jeiintegration-client.toml
    - consider moving all the onShiftAndDebug enabled options into the dev configs, or atleast some of them (translation keys)